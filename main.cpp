#include <iostream>
#include <chrono>
#include "ImageUtils/Image.hpp"
#include <opencv2/opencv.hpp>
#include <iterator>
#include "mandelbrot/plotGenerator.h"
#include "mandelbrot/gpu/gpuGenerator.h"
#include "mandelbrot/cpu/cpuGenerator.h"

using namespace std;

// -2.3 -1.5 3 1000 4 200
int main(int argc, char **argv) {
    double startx, starty, scale;
    int points, threads, iterations;

    if (argc == 7) {
        startx = stod(argv[1]);
        starty = stod(argv[2]);
        scale = stod(argv[3]);
        points = stoi(argv[4]);
        threads = stoi(argv[5]);
        iterations = stoi(argv[6]);
    } else {
        startx = -2.3;
        starty = -1.5;
        scale = 3;
        points = 5000;
        threads = 16;
        iterations = 1;
        printf("Usage: ./mandelbrot [startX] [startY] [scale] [points] [threads] [iterations]");
        //return 0;
    }

    while (scale > 0.0) {
        //PlotGenerator *plot = new CpuGenerator(threads);
        PlotGenerator *plot = new GpuGenerator();

        auto t1 = std::chrono::high_resolution_clock::now();
        auto image = plot->computePlot(startx, starty, scale, points, iterations, 1e6);
        auto t2 = std::chrono::high_resolution_clock::now();
        auto computeDurationMs = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
        std::cout << "Compute: " << computeDurationMs << "ms" << endl;

        namedWindow("image", cv::WINDOW_KEEPRATIO);
        imshow("image", (const cv::Mat) *(image->image));
        cv::resizeWindow("image", 2500, 1440);

        auto k = cv::waitKey(0);

        switch (k) {
            case '+':
                scale *= 0.9;
                break;

            case '-':
                scale *= 1.1;
                break;

            case '4':
                startx -= 0.1 * scale;
                break;

            case '6':
                startx += 0.1 * scale;
                break;

            case '8':
                starty -= 0.1 * scale;
                break;

            case '2':
                starty += 0.1 * scale;
                break;

            case '*':
                points *= 2;
                break;

            case '/':
                points /= 2;
                break;

            case '0':
                iterations *= 2;
                break;

            case '.':
                iterations /= 2;
                break;

            case 's':
                auto writeStart = std::chrono::high_resolution_clock::now();

                image->write("brots/mandelbrot_opencv" + to_string(iterations) + to_string(points) + to_string(scale) +
                             to_string(startx) + to_string(starty) + ".png");
                auto writeEnd = std::chrono::high_resolution_clock::now();
                auto writeDuration = std::chrono::duration_cast<std::chrono::milliseconds>(
                        writeEnd - writeStart).count();
                std::cout << "Write: " << writeDuration << "ms" << endl;

                break;
        }

        delete plot;
    }
    return 0;
}