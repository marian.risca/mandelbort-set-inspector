#include <fstream>
#include "Image.hpp"

using namespace std;
using namespace cv;

/**
 * Create an ImageUtils from a file
 * @param filename The filename to read
 */
Image::Image(int height, int width) {
    image = new Mat(height, width, CV_8UC3);
}

void Image::set(int row, int col, RGBPixel value) {
    image->at<Vec3b>(row, col) = {  value.blue, value.green, value.red };
}

void Image::write(const std::string& file) {
    vector<int> compression_params;
    compression_params.push_back(IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(1);    // 0 to 9
    imwrite(file, *image, compression_params);
}

Image::~Image() {
    delete image;
}


