#pragma once

#include "../Eigen/Dense"
#include "RgbPixel.hpp"
#include "../Eigen/src/Core/util/Constants.h"
#include <opencv2/opencv.hpp>

using Eigen::Matrix;
using Eigen::Dynamic;

class Image {
public:
    Image(int height, int width);
    ~Image();

    void set(int row, int col, RGBPixel value);
    void write(const std::string& destinationFilename);
    cv::Mat* image;
};