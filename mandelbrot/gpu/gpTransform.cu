#include <helper_string.h>
#include <cublas_v2.h>
#include <helper_functions.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <ctime>
#include <chrono>

#define DECISION_ITERATIONS 10

struct CheckDivergence : public thrust::binary_function<float2, float2, int>
{
    __host__ __device__
    int operator()(const float2& val, const float2& point) const
    {
        float2 res = val;
        int i;
        for (i = 0; i < DECISION_ITERATIONS; i++) {
            res.x = res.x * res.x - res.y * res.y + point.x;
            res.y = 2 * res.x * res.y + point.y;

            if (res.x * res.x + res.y * res.y > 1e6) {
                return i;
            }
        }

        return DECISION_ITERATIONS;
    }
};

thrust::host_vector<int> transform(float startX, float startY, float scale, int pts)
{
    const size_t N = pts * pts;
    float pointStep = scale / pts;

    thrust::host_vector<float2> values(N);
    thrust::generate(values.begin(), values.end(), [](){return float2{0, 0};});

    thrust::host_vector<float2> points(N);

    for(int i=0; i<pts; i++) {
        for (int j=0; j<pts; j++) {
            auto x = startX + i * pointStep;
            auto y = startY + j * pointStep;

            points[i * pts + j] = float2{ x, y };
        }
    }

//    for(int i=0; i<pts*pts; i++) {
//            auto xi = i / pts;
//            auto yi = i % pts;
//
//            points[i] = float2{ startX + xi * pointStep, startY + yi * pointStep };
//    }

    auto t111 = std::chrono::high_resolution_clock::now();

    thrust::device_vector<float2> d_A = values;
    thrust::device_vector<float2> d_B = points;

    auto t222 = std::chrono::high_resolution_clock::now();
    auto computeDurationMs3 = std::chrono::duration_cast<std::chrono::milliseconds>(t222 - t111 ).count();

    thrust::host_vector<int> h_result_modified(N);

    thrust::device_vector<int> d_result_modified(N);

    std::cout << "Start compute"<< std::endl;
    auto t1 = std::chrono::high_resolution_clock::now();
    thrust::transform(d_A.begin(), d_A.end(),
                      d_B.begin(), d_result_modified.begin(),
                      CheckDivergence());
    auto t2 = std::chrono::high_resolution_clock::now();
    auto computeDurationMs = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1 ).count();


    auto t11 = std::chrono::high_resolution_clock::now();
    h_result_modified = d_result_modified;

    auto t22 = std::chrono::high_resolution_clock::now();
    auto computeDurationMs1 = std::chrono::duration_cast<std::chrono::milliseconds>(t22 - t11 ).count();
    auto computeDurationTotal = std::chrono::duration_cast<std::chrono::milliseconds>(t22 - t111 ).count();

    std::cout << "Transfer host -> device: " << computeDurationMs3 << "ms" << std::endl;
    std::cout << "Compute CUDA: " << computeDurationMs << "ms" << std::endl;
    std::cout << "Transfer device -> host: " << computeDurationMs1 << "ms" << std::endl;
    std::cout << "Total compute: " << computeDurationTotal << "ms" << std::endl;

    return h_result_modified;
}