#pragma once
#include <thrust/host_vector.h>

thrust::host_vector<int> transform(float startX, float startY, float scale, int points);