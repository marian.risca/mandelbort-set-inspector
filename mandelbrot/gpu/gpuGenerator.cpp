#pragma once

#include <thrust/host_vector.h>
#include "gpuGenerator.h"
#include "gpTransform.h"

Image* GpuGenerator::computePlot(float startX, float startY, float scale, int points, int iterations, unsigned long threshold) {
    printf("Computing mandelbrot set, resolution %dx%d using CUDA, %d iterations\n", points, points, iterations);
    printf("Compute window: X: [%f, %f], Y: [%f, %f], %.2fMPx\n", startX, startX + scale, startY, startY + scale, points * points / 1000000.0);

    if (points != plotX || points != plotY) {
        if (image != nullptr) {
            delete image;
        }

        image = new Image{points, points};
        plotX = points;
        plotY = points;
    }

    thrust::host_vector<int> divergenceVector = transform(startX, startY, scale, points);

    auto t1 = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < points * points; i++) {
        auto row = i / points;
        auto col = i % points;
        auto its = divergenceVector[i];
//        auto c = magnitude / 1000 * 255;
//        image->set(row, col, RGBPixel{0, static_cast<unsigned char>(c) , 0});
        if (its == 10) {
            // convergent
            image->set(row, col, RGBPixel{0, 0, 100});
        }
        else {
            auto intensity = static_cast<unsigned char>((float) its / 10 * 255);
            image->set(row, col, RGBPixel{ 0, intensity, 0 });
        }
    }

    auto t2 = std::chrono::high_resolution_clock::now();
    auto computeDurationMs = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1 ).count();
    std::cout << "Compute: " << computeDurationMs << "ms" << std::endl;

    printf("CUDA Compute finished.\n");
    return image;
}
