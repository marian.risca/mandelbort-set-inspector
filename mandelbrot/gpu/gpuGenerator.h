#pragma once

#include "../plotGenerator.h"
#include "../../ImageUtils/Image.hpp"

class GpuGenerator : public PlotGenerator {
public:
    Image* computePlot(float startX, float startY, float scale, int points, int iterations, unsigned long threshold);
    ~GpuGenerator() = default;
private:
    Image* image = nullptr;
    int plotX;
    int plotY;
};


