#pragma once

#include <future>
#include "../plotGenerator.h"
#include "../../ImageUtils/Image.hpp"

class CpuGenerator: public PlotGenerator {
public:
    explicit CpuGenerator(int workerCount);
    Image* computePlot(float startX, float startY, float scale, int points, int iterations, unsigned long threshold);
    ~CpuGenerator() = default;
private:
    struct ThreadInfo {
        int threadId;
        double scale;
        int iterations;
        unsigned long threshold;
        Image* image;
        std::pair<double, double> plotReference;
        int workerCount;
    };

    static void processBlock(const ThreadInfo& state);

    int workerCount;

    // the number of points in the window to be processed
    int plotX;
    int plotY;
    Image* image = nullptr;

    static std::mutex matrixMutex;
};

