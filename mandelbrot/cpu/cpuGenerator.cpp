#include "cpuGenerator.h"

using namespace std;
using Eigen::Dynamic;
using Eigen::Matrix;
using complex_literals::operator""i;

std::mutex CpuGenerator::matrixMutex;

CpuGenerator::CpuGenerator(const int workerCount) {
    this->workerCount = workerCount;
}

Image* CpuGenerator::computePlot(float startX, float startY, float scale, int points, int iterations, unsigned long threshold) {
    printf("Computing mandelbrot set, resolution %dx%d using %d threads, %d iterations\n", points, points, workerCount, iterations);
    printf("Compute window: X: [%f, %f], Y: [%f, %f], %d MP\n", startX, startX + scale, startY, startY + scale, points * points / 1000000);

    std::vector<std::future<void>> workerFutures;
    workerFutures.reserve(workerCount);

    if (points != plotX || points != plotY) {
        if (image != nullptr) {
            delete image;
        }

        image = new Image{points, points};
        plotX = points;
        plotY = points;
    }



    // the size of a point on the graph
    auto s = scale / points;
    auto plotReference = make_pair(startX, startY);

    // each worker will process the lines where line index % 16 == threadId
    for(auto threadId = 0; threadId < workerCount; threadId++) {
        workerFutures.push_back(async(std::launch::async, processBlock, ThreadInfo {
                threadId,
                s,
                iterations,
                threshold,
                image,
                plotReference,
                workerCount
        }));
    }

    for(auto& thread : workerFutures) {
        thread.get();
    }

    printf("Compute finished.\n");

    return image;
}

void CpuGenerator::processBlock(const ThreadInfo& state) {
    const auto rows = state.image->image->rows;
    const auto cols = state.image->image->cols;

    for (auto i = state.threadId; i < rows; i += state.workerCount) {
        for (auto j = 0; j < cols; j++) {
            complex<double> val(0, 0);
            complex<double> c(state.plotReference.first + i * state.scale,
                              state.plotReference.second + j * state.scale);

            double magnitude = 0;
            int it;
            for (it = 0; it < state.iterations; it++) {
                // v = v^2 + c
                val = pow(val, 2) + c;

                magnitude = norm(val);
                if (magnitude > state.threshold) {
                    break;
                }
            }

            matrixMutex.lock();

            if (magnitude < state.threshold) {
                state.image->set(j, i, RGBPixel{0, 0, 100});
                //state.image->set(y, x, RGBPixel{0, intensity, 0 });
            } else {
                auto intensity = static_cast<unsigned char>((float) it / state.iterations * 255);
                state.image->set(j, i, RGBPixel{0, intensity, 0});
            }

            matrixMutex.unlock();
        }
    }
}