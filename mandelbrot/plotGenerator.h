#pragma once

#include "../ImageUtils/Image.hpp"

class PlotGenerator {
public:
    virtual Image* computePlot(float startX, float startY, float scale, int points, int iterations, unsigned long threshold) = 0;
    virtual ~PlotGenerator() = default;
};