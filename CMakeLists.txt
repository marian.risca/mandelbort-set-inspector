cmake_minimum_required(VERSION 3.14)
project(mandelbrot LANGUAGES CXX CUDA)

set(CMAKE_CXX_STANDARD 20)
LINK_DIRECTORIES(/opt/cuda/targets/x86_64-linux/lib/)

include_directories(/opt/cuda/samples/common/inc)

add_executable(mandelbrot main.cpp
        ImageUtils/Image.cpp ImageUtils/Image.hpp
        ImageUtils/RgbPixel.cpp ImageUtils/RgbPixel.hpp
        mandelbrot/gpu/gpTransform.h mandelbrot/gpu/gpTransform.cu
        mandelbrot/plotGenerator.h
        mandelbrot/cpu/cpuGenerator.cpp mandelbrot/cpu/cpuGenerator.h
        mandelbrot/gpu/gpuGenerator.cpp mandelbrot/gpu/gpuGenerator.h)

find_package(Threads REQUIRED)
find_package(OpenCV REQUIRED)
find_package(CUDA REQUIRED)
find_package(Thrust REQUIRED CONFIG)

include_directories("${CUDA_INCLUDE_DIRS}")

thrust_create_target(Thrust)


target_link_libraries(mandelbrot PRIVATE -lcublas Threads::Threads ${OpenCV_LIBS} Thrust)